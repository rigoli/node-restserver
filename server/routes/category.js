const express = require('express');
const _ = require('underscore');
const { verifyToken, isAdmin } = require('../middlewares/authentication');
const Category = require('../models/category');

let app = express();

//=============================
// Mostrar todas las categorias
//=============================
app.get('/categories', verifyToken, (req, res) => {
    let desde = new Number(req.query.desde || 0);
    let limite = new Number(req.query.limite || 0);

    Category.find({ del: false }, 'description user')
        .sort('description')
        .populate('user', 'name email role')
        .skip(desde)
        .limit(limite)
        .exec((err, categoriesDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Category.countDocuments({ del: false }, (err, numRows) => {
                res.json({
                    ok: true,
                    numRows,
                    categoriesDB
                });
            });
        });
});

//=============================
// Mostrar una categoria por ID
//=============================
app.get('/category/:id', (req, res) => {
    let id = req.params.id;

    Category.findById(id, (err, categoryDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!categoryDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Categoria no encontrada'
                }
            });
        }

        res.json({
            ok: true,
            user: categoryDB
        });
    });
});

//=============================
//    Crear nueva categoria
//=============================
app.post('/category', [verifyToken, isAdmin], (req, res) => {
    let body = req.body;

    let category = new Category({
        description: body.description,
        user: req.user._id
    });

    category.save((err, categoryDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            category: categoryDB
        });
    });
});

//=============================
//   Actualizar categorias
//=============================
app.put('/category/:id', [verifyToken, isAdmin], (req, res) => {
    let id = req.params.id;
    // let body = _.pick(req.body, ['description']);
    let body = req.body;

    let descCategory = {
        description: body.description
    }

    Category.findByIdAndUpdate(id, descCategory, {
        new: true, //devuelve el objeto actualizado
        runValidators: true, //aplica las validaciones del esquema del modelo
        context: 'query' //necesario para las disparar las validaciones de mongoose-unique-validator
    }, (err, categoryDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!categoryDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Categoria no encontrada'
                }
            });
        }

        res.json({
            ok: true,
            user: categoryDB
        });
    });
});

//=============================
//    Eliminar categorias
//=============================
app.delete('/category/:id', [verifyToken, isAdmin], (req, res) => {
    let id = req.params.id;

    Category.findByIdAndRemove(id, (err, catDel) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!catDel) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'La categoria no fue encontrada'
                }
            });
        }

        res.json({
            ok: true,
            catDel,
            message: 'La categoria fue borrada'
        });
    });
});

module.exports = app;