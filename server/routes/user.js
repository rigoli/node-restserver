const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');

const User = require('../models/user');
const { verifyToken, isAdmin } = require('../middlewares/authentication');

const app = express();

app.get('/users', verifyToken, (req, res) => {
    let desde = new Number(req.query.desde || 0);
    let limite = new Number(req.query.limite || 0);

    User.find({ del: false }, 'name email role estado google img')
        .skip(desde)
        .limit(limite)
        .exec((err, users) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            User.countDocuments({ del: false }, (err, numRows) => {
                res.json({
                    ok: true,
                    numRows,
                    users
                });
            });
        });
});

app.post('/user', [verifyToken, isAdmin], (req, res) => {
    let body = req.body;

    let user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    user.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: usuarioDB
        });
    });
});

app.put('/user/:id', [verifyToken, isAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'img', 'role', 'estado']);

    User.findByIdAndUpdate(id, body, {
        new: true, //devuelve el objeto actualizado
        runValidators: true, //aplica las validaciones del esquema del modelo
        context: 'query' //necesario para las disparar las validaciones de mongoose-unique-validator
    }, (err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!userDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            user: userDB
        });
    });
});

app.delete('/user/:id', [verifyToken, isAdmin], function(req, res) {
    let id = req.params.id;

    // ELIMINA MEDIANTE CAMBIO DE ESTADO
    User.findByIdAndUpdate(id, { del: true }, { new: true }, (err, userDel) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!userDel) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            user: userDel
        });
    });
});

module.exports = app;