const express = require('express');
const { verify } = require('jsonwebtoken');
const { verifyToken, isAdmin } = require('../middlewares/authentication');
const Product = require('../models/product');

let app = express();

app.get('/products', verifyToken, (req, res) => {
    let desde = new Number(req.query.desde || 0);
    let limite = new Number(req.query.limite || 0);

    Product.find({ avaliable: true, del: false }, 'name description')
        .sort('description')
        .populate('user', 'name email role')
        .populate('category', 'description')
        .skip(desde)
        .limit(limite)
        .exec((err, productDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Product.countDocuments({ del: false }, (err, numRows) => {
                res.json({
                    ok: true,
                    numRows,
                    productDB
                });
            });
        });
});

app.get('/product/:id', (req, res) => {
    let id = req.params.id;

    Product.findById(id)
        .populate('user', 'name email role')
        .populate('category', 'description')
        .exec((err, productDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if (!productDB) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'El producto no encontrado'
                    }
                });
            }

            res.json({
                ok: true,
                user: productDB
            });
        });
});

app.post('/product', [verifyToken, isAdmin], (req, res) => {
    let body = req.body;

    let product = new Product({
        name: body.name,
        priceUni: body.priceUni,
        description: body.description,
        avaliable: body.avaliable,
        category: body.category,
        user: req.user._id
    });

    product.save((err, productDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            product: productDB
        });
    });
});

app.get('/products/search/:term', verifyToken, (req, res) => {
    let term = req.params.term;
    let regex = new RegExp(term, 'i');

    Product.find({ name: regex })
        .populate('categoria', 'description')
        .exec((err, products) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if (!products) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'El producto no encontrado'
                    }
                });
            }

            res.json({
                ok: true,
                products
            });
        });
});

app.put('/product/:id', [verifyToken, isAdmin], (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let product = {
        name: body.name,
        priceUni: body.priceUni,
        description: body.description,
        avaliable: body.avaliable,
        category: body.category,
        user: req.user._id
    }

    Product.findByIdAndUpdate(id, product, {
        new: true, //devuelve el objeto actualizado
        runValidators: true, //aplica las validaciones del esquema del modelo
        context: 'query' //necesario para las disparar las validaciones de mongoose-unique-validator
    }, (err, productDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El producto no existe'
                }
            });
        }

        res.json({
            ok: true,
            user: productDB
        });
    });
});

app.delete('/product/:id', [verifyToken, isAdmin], (req, res) => {
    let id = req.params.id;

    Product.findById(id, (err, productDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'La categoria no fue encontrada'
                }
            });
        }

        productDB.avaliable = false;

        productDB.save((err, productDelete) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                product: productDelete,
                message: 'Producto borrado',

            });
        });
    });
});

module.exports = app;