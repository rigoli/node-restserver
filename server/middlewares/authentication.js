const jwt = require('jsonwebtoken');

// =========================
// Verificar Token
// =========================
module.exports.verifyToken = (req, res, next) => {
    let token = req.get('token');

    // console.log(token);

    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            });
        }

        req.user = decoded.user;
        next();
    });
};

// =========================
// IS ADMIN
// =========================
module.exports.isAdmin = (req, res, next) => {
    let user = req.user;

    if (user.role === 'ADMIN_ROLE') {
        next();
    } else {
        return res.json({
            ok: false,
            err: {
                message: 'El usuario no es administrador'
            }
        });
    }
};