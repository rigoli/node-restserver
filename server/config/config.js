//  =====================================
//  Puerto NODEJS
//  =====================================
process.env.PORT = process.env.PORT || 3000;

//  =====================================
//  Entorno
//  =====================================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//  =====================================
//  Vencimiento del Token
//  =====================================
//
// 60 segundos
// 60 minutos
// 24 hrs
// 30 dias
process.env.EXPIRATION_TOKEN = '24h';
// process.env.EXPIRATION_TOKEN = 60 * 60 * 24 * 30;

//  =====================================
//  SEED de auth
//  =====================================
process.env.SEED = process.env.SEED || 'seed';

//  =====================================
//  Base de datos
//  =====================================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/cafe';
} else {
    urlDB = process.env.MONGO_DB;
}

process.env.URLDB = urlDB;


//  =====================================
//  Google ClientID
//  =====================================
process.env.CLIENT_ID = process.env.CLIENT_ID || '620130291011-4a0e8nhkh3a5fp7vnpu8g3mc8m8nj1ng.apps.googleusercontent.com';